package com.example.micro_service_employee_tpfinal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEmployeeDto {

    private int id;

    private String lastName;
    private String firstName;

    private String password;



    public ResponseEmployeeDto(String lastName, String firstName, String password) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
    }
}
