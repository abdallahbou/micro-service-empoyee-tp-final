package com.example.micro_service_employee_tpfinal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestEmployeeDto {

    private String lastName;
    private String firstName;

    private String password;
}
