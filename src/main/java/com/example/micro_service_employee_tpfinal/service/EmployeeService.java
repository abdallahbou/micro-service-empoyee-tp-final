package com.example.micro_service_employee_tpfinal.service;

import com.example.micro_service_employee_tpfinal.dto.RequestEmployeeDto;
import com.example.micro_service_employee_tpfinal.dto.ResponseEmployeeDto;
import com.example.micro_service_employee_tpfinal.entity.Employee;
import com.example.micro_service_employee_tpfinal.exception.NotFoundException;

import java.util.List;

public interface EmployeeService {

    List<ResponseEmployeeDto> findAllEmployee();

    ResponseEmployeeDto getEmployeebyId(int id);

    ResponseEmployeeDto createEmployee(RequestEmployeeDto employeeDto);

    ResponseEmployeeDto getEmployee(String search) throws NotFoundException;

    void deleteEmployeeById(int id);
}
