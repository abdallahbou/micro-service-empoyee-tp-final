package com.example.micro_service_employee_tpfinal.service;

import com.example.micro_service_employee_tpfinal.dto.RequestEmployeeDto;
import com.example.micro_service_employee_tpfinal.dto.ResponseEmployeeDto;
import com.example.micro_service_employee_tpfinal.entity.Employee;
import com.example.micro_service_employee_tpfinal.exception.NotFoundException;
import com.example.micro_service_employee_tpfinal.repository.EmployeeRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository _repository;

    @Autowired
    private ModelMapper _modelMapper;



    @Override
    public List<ResponseEmployeeDto> findAllEmployee() {
        List<Employee> employees = (List<Employee>) _repository.findAll();

        List<ResponseEmployeeDto> dtoList = new ArrayList<>();
        for (Employee e : employees) {

            ResponseEmployeeDto dto = _modelMapper.map(e,ResponseEmployeeDto.class);
            dtoList.add(dto);

        }

        return dtoList;


    }

    @Override
    public ResponseEmployeeDto getEmployeebyId(int id) {

        return _modelMapper.map(findEmployee(id), ResponseEmployeeDto.class);
    }

    @Override
    public ResponseEmployeeDto createEmployee(RequestEmployeeDto employeeDto) {
        Employee employee = _modelMapper.map(employeeDto, Employee.class);
        ResponseEmployeeDto responseEmployeeDto = _modelMapper.map(_repository.save(employee),ResponseEmployeeDto.class);
        return responseEmployeeDto;

    }

    @Override
    public ResponseEmployeeDto getEmployee(String search) throws NotFoundException {
        Employee employee = _repository.searchEmployee(search);
        if( employee == null){
            throw new NotFoundException();
        }
        return _modelMapper.map(employee,ResponseEmployeeDto.class);
    }

    @Override
    public void deleteEmployeeById(int id) {

        Employee employee = findEmployee(id);
        _repository.deleteById(id);

    }

    private Employee findEmployee(int id){
        Employee employee = _repository.findById(id).get();
        return employee;
    }


}
