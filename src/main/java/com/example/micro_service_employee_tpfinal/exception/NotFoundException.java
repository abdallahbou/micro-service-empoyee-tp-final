package com.example.micro_service_employee_tpfinal.exception;

public class NotFoundException extends Exception{

    public NotFoundException(){super("Not Found employee");}
}
