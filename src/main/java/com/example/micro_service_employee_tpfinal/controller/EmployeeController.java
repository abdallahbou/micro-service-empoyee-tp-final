package com.example.micro_service_employee_tpfinal.controller;

import com.example.micro_service_employee_tpfinal.dto.RequestEmployeeDto;
import com.example.micro_service_employee_tpfinal.dto.ResponseEmployeeDto;
import com.example.micro_service_employee_tpfinal.exception.NotFoundException;
import com.example.micro_service_employee_tpfinal.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {


    private  final EmployeeService _employeeService;


    public EmployeeController(EmployeeService employeeService) {
        _employeeService = employeeService;
    }


    @PostMapping("")
    public ResponseEntity<ResponseEmployeeDto> createEmployee(@RequestBody RequestEmployeeDto requestEmployeeDto){
        return new ResponseEntity<>(_employeeService.createEmployee(requestEmployeeDto), HttpStatus.CREATED);
    }


    @GetMapping("")
    public ResponseEntity<List<ResponseEmployeeDto>> getAllEmployee(){
        return new ResponseEntity<>(_employeeService.findAllEmployee(),HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseEmployeeDto> getEmployeeById(@PathVariable int id){
        return  ResponseEntity.ok(_employeeService.getEmployeebyId(id));
    }


    @DeleteMapping("{id}")
    public ResponseEntity<ResponseEmployeeDto> deleteEmployee(@PathVariable int id){

        _employeeService.deleteEmployeeById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/search/{search}")
    public ResponseEntity<ResponseEmployeeDto> get(@PathVariable String search)

    {
        try {
            return ResponseEntity.ok(_employeeService.getEmployee(search + "%"));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
