package com.example.micro_service_employee_tpfinal.repository;

import com.example.micro_service_employee_tpfinal.entity.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Integer> {

    @Query("SELECT c FROM Employee c where c.firstName like :search or c.lastName like :search")
    public Employee searchEmployee(@Param("search")String search);


}
