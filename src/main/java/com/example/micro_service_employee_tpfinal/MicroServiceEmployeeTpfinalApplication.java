package com.example.micro_service_employee_tpfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroServiceEmployeeTpfinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServiceEmployeeTpfinalApplication.class, args);
	}

}
